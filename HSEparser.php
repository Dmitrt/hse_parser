<?php

require 'vendor/autoload.php';

class HseParser
{
    private $institution = [];
    private $language = 'en';
    
    public function getPrograms()
    {
        return $this->institution['Programs'];
    }
    
    public function parse()
    {   
        $this->language = 'en';
        
        echo '  [ parseStart ]: InstitutionPage ' . PHP_EOL;
        $start = microtime(true);
        $this->InstitutionParse();
        echo '<br>' . '  [ parseSuccess ]: InstitutionPage time: ' . (microtime(true) - $start) . ' sec' . PHP_EOL;
        
        echo '<br>' . '  [ parseStart ]: UnitsPage ' . PHP_EOL;
        $start = microtime(true);
        $this->UnitsParse();
        echo '<br>' . '  [ parseSuccess ]: UnitsPage time: ' . (microtime(true) - $start) . ' sec' . PHP_EOL;
        
        echo '<br>' . '  [ parseStart ]: Campus ' . PHP_EOL;
        $start = microtime(true);
        $this->campusParse();
        echo '<br>' . '  [ parseSuccess ]: Campus time: ' . (microtime(true) - $start) . ' sec' . PHP_EOL;
        
        //echo '<br>' . ' [ parseStart ]: courses ' . PHP_EOL;
        //$start = microtime(true);
        //$this->coursesParse();
        //echo '<br>' . '[ parseSuccess ]: courses time: ' . (microtime(true) - $start) . ' sec' . PHP_EOL;
        
        //echo '<br>' . ' [ parseStart ]: programs ' . PHP_EOL;
        //$start = microtime(true);
        //$this->programsParse();
        //echo '<br>' . '[ parseSuccess ]: programs time: ' . (microtime(true) - $start) . ' sec' . PHP_EOL;
        
        //echo '<br>' .'  [ parseStart ]: Employee ' . PHP_EOL;
        //$start = microtime(true);
        //$this->EmployeeParseEn();
        //echo '<br>' . '  [ parseSuccess ]: Employee time: ' . (microtime(true) - $start) . ' sec' . PHP_EOL;
        
        $this->institutionInformation();
        //$this->coursesExport();
        $this->institutionExport();
        //$this->programsExport();
        //$this->employeeExport();
    }
    
    public function institutionParse()
    {   
        $url = false;
        if($this->language == 'en'){
            $url = 'https://www.hse.ru/en/org/hse/info/';
        }else{
            $url = 'https://www.hse.ru/org/hse/info/';
        }
        $html = file_get_html($url);
        $elements = $html->find('a[class=link no-visited]');
        $this->institution['Name'] = (string) $elements[0]->plaintext;
        $tmpInformation = $html->find('div[class=content__inner]');
        $description = $tmpInformation[1]->find('p');
        $this->institution['Description'] = (string) $description[1]->plaintext;
        $this->institution['WebPage'] = 'https://www.hse.ru/';
        $tmpInformation = $html->find('dl[class=first_child last_child]',0);
        if($this->language == 'en'){
            $this->institution['Adress'] = (string) $tmpInformation->find('p')[2]->nodes[3];
        }else{
            $this->institution['Adress'] = (string) $tmpInformation->find('p')[1]->nodes[2];
        }
        $html->clear();
        unset($html);
    }
    
    private function institutionExport()
    {
        $dom = new domDocument('1.0', 'utf-8');
        $dom->formatOutput = true;
        $xmlInstitution = $dom->createElement('HseInstitution');
        $xmlInstitution->setAttribute('Name', $this->institution['Name']);
        $xmlInstitution->setAttribute('WebPage', $this->institution['WebPage']);
        $xmlInstitution->setAttribute('Adress', $this->institution['Adress']);
        $xmlInstitution->setAttribute('Description', $this->institution['Description']);
        //units
        $units = $this->institution['Units'];
        $xmlUnits = $dom->createElement('Units');
        foreach ($units as $unit) {
            $xmlUnit = $dom->createElement('Unit');
            foreach ($unit as $key => $value) {
                $xmlUnit->setAttribute($key, $value);
            }
            $xmlUnits->appendChild($xmlUnit);
        }
        $xmlInstitution->appendChild($xmlUnits);
        unset($units);
        //campus
        $campus = $this->institution['Campus'];
        $xmlCampus = $dom->createElement('Campus');
        if($this->language == 'en'){
            foreach ($campus as $building) {
                $xmlBuilding = $dom->createElement('Building');
                foreach ($building as $key => $element) {
                    if($key == 'Departments' || $key == 'ProgramOffices'){
                        $str = 'Department';
                        if($key == 'ProgramOffices'){
                            $str = 'ProgramOffice';
                        }
                        foreach ($element as $value) {
                            $xmlElement = $dom->createElement($str);
                            foreach($value as $attr=>$val){
                                $xmlElement->setAttribute($attr, $val);
                            }
                            $xmlBuilding->appendChild($xmlElement);
                        }
                    }else{
                        $xmlBuilding->setAttribute($key, $element);
                    }
                }
                $xmlCampus->appendChild($xmlBuilding);
            }
        }else{
            foreach ($campus as $city=>$buildings) {
                foreach ($buildings as $adress => $building) {
                    $xmlBuilding = $dom->createElement('Building');
                    $xmlBuilding->setAttribute('City', $city);
                    $xmlBuilding->setAttribute('Adress', $adress);
                    foreach ($building as $key => $departments) {
                        if($key == 'Подразделения' || $key == 'Учебные офисы образовательных программ'){
                            $str = 'Подразделение';
                            if($key == 'Учебные офисы образовательных программ'){
                                $str = 'УчебныйОфис';
                            }
                            $xmlElement = $dom->createElement($str);
                            foreach ($departments as $department) {
                                foreach ($department as $attr => $value) {
                                    $xmlElement->setAttribute($this->stringToTag($attr), $value);
                                }
                            }
                            $xmlBuilding->appendChild($xmlElement);
                        }else{
                            $xmlElement = $dom->createElement('Здание');
                            foreach ($departments as $department) {
                                foreach ($department as $attr => $value) {
                                    $xmlElement->setAttribute($this->stringToTag($attr), $value);
                                }
                            }
                            $xmlBuilding->appendChild($xmlElement);
                        }
                    }
                    $xmlCampus->appendChild($xmlBuilding);
                }
            }
        }
        
        $xmlInstitution->appendChild($xmlCampus);
        $dom->appendChild($xmlInstitution);
        if($this->language == 'en'){
            $dom->save('xmlResult/Institution.xml');
        }else{
            $dom->save('xmlResult/InstitutionRu.xml');
        }
        unset($dom);
    }
    
    public function unitsParse()
    {
        if($this->language == 'en'){
            $html = file_get_html('https://www.hse.ru/en/education/faculty/');
        }else{
            $html = file_get_html('https://www.hse.ru/education/faculty/');
        }
        $content = $html->find('div.post__text',0);
        $city = '';
        foreach ($content->children() as $element) {
            // 0- school, 1- faculty, 2- department.
            if($element->tag == 'p'){
                if($p = $element->find('a')){
                    foreach ($p as $link) {
                        //чтобы убрать пустые ссылки
                        $str = str_replace('&nbsp;', '', $link->plaintext);
                        $str = trim($str);
                        if($str == ''){continue;}
                        $name = trim($link->plaintext);
                            $this->institution['Units'][] = [
                            'City'=>$city,
                            'Name'=>$name,
                            'Link'=>$link->href,
                            'Type'=>$this->unitType($name),
                        ];
                    }
                }
                if($this->language == 'en'){
                    if($element->children(0) && $element->children(0)->tag == 'strong' && count($element->children(0)->children()) == 0 ){
                        if($element->children(0)->plaintext != 'RESEARCH INSTITUTES AND CENTRES'){
                            $city = $element->children(0)->plaintext;
                        }
                    }
                }
            }elseif ($element->tag == 'ul') {
                foreach ($element->children as $li) {
                    $links = $li->find('a');
                    foreach ($links as $link) {
                        $name = trim($link->plaintext);
                        if($name == ''){continue;}
                        $this->institution['Units'][] = [
                            'City' => $city,
                            'Name' => $name,
                            'Link' => $link->href,
                            'Type' => $this->unitType($name),
                        ];
                    }
                }
            }elseif($element->tag == 'strong'){
                if($link = $element->find('a',0)){
                    if($link->name != 'spb'){
                        $name = trim($element->plaintext);
                        $this->institution['Units'][] = [
                            'City' => $city,
                            'Name' => $name,
                            'Link' => $link->href,
                            'Type' => $this->unitType($name),
                        ];
                    }else{
                        $city = $element->plaintext;
                    }
                }else{
                        $city = $element->plaintext;
                }
            }
        }
        $content->clear();
        unset($content);
        $html->clear();
        unset($html);
    }
    
    public function campusParse()
    {
        if($this->language == 'en'){
            $this->campusParseEn();
        }elseif($this->language == 'ru' || $this->language == 'russian'){
            $this->campusParseRu();
        }
    }
    
    public function campusParseRu()
    {
        $urls = [
            'Moskov'=>'https://www.hse.ru/buildinghse/list',
            'Novgorod'=>'https://www.hse.ru/buildinghse/nnov',
            'Perm'=>'https://www.hse.ru/buildinghse/perm',
            'Saint Petersburg'=>'https://www.hse.ru/buildinghse/spb'
        ];
        foreach ($urls as $city=>$url) {
            $html = file_get_html($url);
            $content = $html->find('div.address_list',0);
            $adress = '';
            foreach ($content->children() as $value) {
                
                if($value->tag == 'h2' && ($value->class == 'with-indent' || $value->class == 'with-indent2') ){
                    $adress = $value->nodes[0]->plaintext;
                }
                
                if($value->tag == 'h3' && $value->class == 'with-indent0'){
                    $name = $adress;
                    $this->institution['Campus'][$city][$value->nodes[0]->plaintext]['Building'][] = ['Name' => $name];
                }
                
                if($value->tag == 'div' && $value->class == 'columns small'){
                    
                    foreach ($value->children() as $column) {
                        $list = $column->find('ul',0);
                        if($list){
                            $type = trim($column->children(0)->plaintext,' :');
                            foreach($list->children() as $unit){
                                $name = '';
                                $link='';
                                if($unit->children(0)){
                                    $this->institution['Campus'][$city][$adress][$type][]=[
                                        'Name'=>$unit->children(0)->plaintext,
                                        'Link'=>$unit->children(0)->href,
                                    ];
                                }else{
                                    $this->institution['Campus'][$city][$adress][$type][] = ['Name'=>$unit->nodes[0]->plaintext];
                                }
                            }
                            $list->clear();
                            unset($list);
                        }
                    }
                }
                
            }
            $content->clear();
            $html->clear();
            unset($content);
            unset($html);
        }
        unset($urls);
    }
    
    public function campusParseEn()
    {
        $url = 'https://www.hse.ru/en/buildinghse/list';
        $html = file_get_html($url);
        
        $content = $html->find('table',0);
        $tableRows = $content->find('tr'); 
        
        foreach ($tableRows as $row) {
            
            $adress = $row->children(0)->find('span',0);
            if($adress){
                
                $building = [];
                $building['Addres'] = $adress->plaintext;
                
                //departments column
                $departments = $row->find('td.p2',0);
                foreach ($departments->children() as $department) {
                    $unit = false;
                    if(count($department->children()) > 0){
                        $link = $department->children(0)->find('a',0);
                        $unit=[
                            'Name'=>$department->children(0)->plaintext,
                            'Type'=>$this->unitType( (string) $department->children(0)->plaintext),
                        ];
                        if($link){
                            $unit['Link'] = $link->href;
                        }else{
                            $unit['Name'] = $department->plaintext;
                        }
                        unset($link);
                    }else{
                        if(mb_strlen($department->plaintext) > 2 ){
                            $unit=[
                                'Name'=>$department->plaintext,
                                'Type'=>$this->unitType($department->plaintext),
                            ];
                        }
                    }
                    if($unit){
                        $building['Departments'][] = $unit;
                    }
                    unset($unit);
                }
                
                //Study Offices of Educational Programmes
                $offices = $row->find('td.p5',0);
                foreach ($offices->children() as $office) {
                    if(count($office->children()) > 0){
                        $building['ProgramOffices'][]=[
                            'Name'=>$office->children(0)->plaintext,
                            'Link'=>$office->children(0)->href,
                        ];
                    }else {
                        if(mb_strlen($office->plaintext) > 2){
                            $building['ProgramOffices'][]=[
                                'Name'=>$office->plaintext,
                            ];
                        }
                    }
                }
                $this->institution['Campus'][] = $building;
                $adress->clear();
                $departments->clear();
                $offices->clear();
                unset($departments);
                unset($offices);
                unset($building);
            }
            unset($adress);
        }
        $content->clear();
        $html->clear();
        unset($tableRows);
        unset($content);
        unset($html);
        unset($url);
    }
    
    public function programsParse()
    {
        //инфа о магистерских уже есть на этой же странице. просто не отображается на сайте
        //когда активны бакалаврские
        if($this->language == 'en'){
            $html = file_get_html('https://www.hse.ru/en/education/programs/#bachelor');
        }else{
            $html = file_get_html('https://www.hse.ru/education/programs/#bachelor');
        }
        $content = $html->find('div.education-content',0);
        foreach ($content->children() as $key=>$programsList) {
            //шапка таблицы не нужна
            if($key == 0){ continue;}
            
            $specialization = $programsList->find('h3.edu-programm__title',0)->plaintext;
            $programs = $programsList->find('div.edu-programm__item');
            foreach($programs as $element){
                $program = [
                    'Name' => $element->children(4)->children(0)->plaintext,
                    'Specialization' => $specialization,
                    'Campus' => $element->children(1)->children(0)->plaintext,
                    'Unit' => $element->children(4)->children(1)->plaintext,
                    'Duration' => $element->children(2)->nodes[0]->plaintext,
                    'Link' => $element->children(4)->children(0)->href,
                ];
                
                if(isset($element->children(4)->children(0)->href)){
                    if($courses = $this->programCoursesParse($element->children(4)->children(0)->href . 'courses/')){
                        $program['Courses'] = $courses;
                        unset($courses);
                    }
                    if($admission = $this->programAdmissionParse($element->children(4)->children(0)->href . 'admission/')){
                        $program['Admission'] = $admission;
                        unset($admission);
                    }
                }
                
                $this->institution['Programs'][] = $program;
                unset($program);
                
            }
            unset($programs);
            unset($specialization);
        }
        $content->clear();
        $html->clear();
        unset($content);
        unset($html);
    }
    
    public function programCoursesParse($url)
    {
        $html = file_get_html($url);
        //курсы с первой страницы
        $result = $this->programCoursesPageParse($url);
        //если есть пагинация
        $links = $html->find('div.pages',0);
        if($links){
            foreach($links->find('a') as $link) {
                $courses = $this->programCoursesPageParse($link->href);
                foreach($courses as $course){
                    $result[] = $course;
                    unset($course);
                }
            }
            $links->clear();
        }
        $html->clear();
        unset($links);
        unset($html);
        return $result;
    }
    
    private function programCoursesPageParse($url)
    {
        $html = file_get_html($url);
        $result = [];
        if(isset($html)){
            
            $coursesTable = $html->find('div.edu-events',0);
            if(isset($coursesTable)){
                foreach($coursesTable->children() as $index=>$courseRow){
                    
                    if($index == 0){ continue; }
                    
                    $course = [];
                    if($courseTitle = $courseRow->find('div.edu-events_courses',0)){
                        $course['Name'] = $courseTitle->children(0)->children(0)->plaintext;
                        
                        $status = $courseTitle->find('div.edu-program_status',0);
                        $course['Status'] = $courseTitle->children(1)->plaintext;
                        $status->clear();
                        unset($status);
                        
                        if($languages = $courseTitle->children(2)->children()){
                            foreach($languages as $language){
                                $course['Languages'][] = $language->plaintext;
                            }
                            unset($languages);
                        }
                        
                        $course['Link'] = $courseTitle->children(0)->children(0)->href;
                        $courseTitle->clear();
                        unset($courseTitle);
                    }
                    
                    $modules = $courseRow->find('div.edu-events_modules',0);
                    if(isset($modules) && $modules->children()){
                        $time = $modules->children(0)->attr['data-tooltip'];
                        
                        if(preg_match('/([\-0-9\, ]+)[year]{4}/u', $time, $matches) || preg_match('/([\-0-9\, ]+)[\-йкурс ]{7}/u', $time, $matches)){
                            $course['Year'] = trim($matches[1],' ,');
                        }
                        if(preg_match('/([\-0-9\, ]+)[module]{6}/u', $time, $matches) || preg_match('/([\-0-9\, ]+)[модуль]{6}/u', $time, $matches)){
                            $course['Modules'] = trim($matches[1],' ,');
                        }
                        if(preg_match('/([\-0-9\, ]+)[semester]{8}/u', $time, $matches) || preg_match('/([\-0-9\, ]+)[семестр]{7}/u', $time, $matches)){
                            $course['Semester'] = trim($matches[1],' ,');
                        }
                        $modules->clear();
                        unset($time);
                    }
                    unset($modules);
                    
                    //$files = $courseRow->find('div.edu-events_files',0);
                    //if(isset($files)){
                    //    foreach($files->children() as $file){
                    //        $course['Files'][] = 'https://www.hse.ru' . $file->children(0)->children(1)->href;
                    //    }
                    //    $files->clear();
                    //}
                    //unset($files);
                    
                    //$instructors = $courseRow->find('div.edu-events_persons',0);
                
                    $result[] = $course;
                    unset($course);
                }
                $coursesTable->clear();
            }
            
            $html->clear();
            unset($coursesTable);
            unset($html);
            return $result;
        }
    }
    
    private function programAdmissionParse($url)
    {
        $result = false;
        if($html = file_get_html($url)){
            if($admissionTable = $html->find('div.b-program__properties',0)){
                foreach($admissionTable->children() as $row){
                    $name = $row->children(0);
                    $value = $row->children(1);   
                    if(count($value->children()) == 1){
                        if($value->children(0)->tag == 'p'){
                            
                            if(count($value->children(0)->children())==3){
                                $result['Unit'] = $value->children(0)->children(2)->plaintext;
                                $result['UnitLink'] = $value->children(0)->children(2)->href;
                            }else{
                                $result[$name->plaintext] = $value->plaintext;
                            }
                        }elseif($value->children(0)->tag == 'ol'){
                            foreach ($value->children(0)->children() as $li) {
                                if($li->children(0)){
                                    if($li->children(0)->tag == 'span'){
                                        $result['Entrance exam'][$li->nodes[0]->plaintext] = $li->children(0)->plaintext;
                                    }
                                }else{
                                    $result[$name->plaintext][] = $li->plaintext;
                                }
                            }
                        }
                    }
                    if(count($value->children()) >= 2){
                        if($languages = $value->find('span.b-program__lang2')){
                            foreach ($languages as $index => $language) {
                                $result['Languages'][] = $language->plaintext;
                            }
                        }
                        if($element = $value->find('span.b-program_big',0)){
                            if($element->class =='b-program_big b-program_fraction') {
                                if($sup = $element->find('sup',0)){
                                    $result['FreePositions'] = $sup->plaintext;
                                }
                                if($sub = $element->find('sub',0)){
                                    $result['PaidPositions'] = $sub->plaintext;
                                }
                            }else{
                                $result['Duration'] = $element->plaintext;
                            }
                                    
                        }
                        if($value->children(3)){
                            $result['Type'] = $value->children(3)->plaintext;
                        }
                        if($value->children(0)->tag == 0){
                            $result['Tuition Fee'] = $value->children(0)->plaintext;
                        }
                    }
                }
                $admissionTable->clear();
            }
            
            $html->clear();
            unset($admissionTable);
            unset($html);
            return $result;
        }
    }
    
    public function coursesParse()
    {
        if($this->language == 'en'){
            $this->institution['Courses'] = $this->coursesParseEn();
        }elseif($this->language == 'ru' || $this->language == 'russian'){
            $this->institution['Courses'] = $this->coursesParseRu();
        }
    }
    
    public function coursesExport()
    {
        if($this->language == 'en'){
            $this->coursesExportEn();
        }elseif($this->language == 'ru' || $this->language == 'russian'){
            $this->coursesExportRu();
        }
    }
    
    public function coursesParseEn()
    {
        $urls = [
            'Moskow'=>'https://www.hse.ru/en/edu/study/branch=135213?level=&branch=22723&faculty=&part=&author=',
            'Saint Petersburg'=>'https://www.hse.ru/en/edu/study/branch=135213?level=&branch=135083&faculty=&part=&author=',
            'Perm'=>'https://www.hse.ru/en/edu/study/branch=135213?level=&branch=135213&faculty=&part=&author=',
            'Nizhny Novgorod'=>'https://www.hse.ru/en/edu/study/branch=135213?level=&branch=135288&faculty=&part=&author=',
        ];
        $courses = [];
        foreach ($urls as $city => $url) {
            $html = file_get_html($url);
            $content = $html->find('div.dynamic-content',0);
            if($content->children()){
                foreach ($content->children() as $index => $row) {
                    //пропуск шапки таблицы и комментариев.
                    if($index == 0 || $row->tag == 'comment'){
                        continue;
                    }
                    
                    $course = [];
                    $modules = $row->children(0);
                    $title = $modules->children(0);
                    if($title){
                        $course['Name'] = $title->children(0)->plaintext;
                        $course['CourseLink'] = 'http://www.hse.ru' . $title->children(0)->href;
                        $course['Campus'] = $city;
                    }
                    
                    $ul = $modules->children(1);
                    switch ($modules->class) {
                        case 'block  single':
                            $course['Modules'] = '1';
                            break;
                        case 'block  double':
                            $course['Modules'] = '1-2';
                            break;
                        case 'block  triple':
                            $course['Modules'] = '1-3';
                            break;
                        case 'block   ':
                            $course['Modules'] = '1-4';
                            break;
                        case 'block empty-single-before single':
                            $course['Modules'] = '2';
                            break;
                        case 'block empty-single-before double':
                            $course['Modules'] = '2-3';
                            break;
                        case 'block empty-single-before triple':
                            $course['Modules'] = '2-4';
                            break;
                        case 'block empty-double-before single':
                            $course['Modules'] = '3';
                            break;
                        case 'block empty-double-before double':
                            $course['Modules'] = '3-4';
                            break;
                        case 'block empty-triple-before single':
                            $course['Modules'] = '4';
                            break;
                        default:
                            break;
                    }
                    
                    if($ul){
                        foreach ($ul->children() as $li) {
                            if($li->children(0)->tag == 'span'){
                                $type = trim($li->children(0)->plaintext,' :');
                                if(count($li->children()) == 1){
                                    $course[$type] = trim($li->nodes[1]->plaintext,' ');
                                }else{
                                    $course[$type] = $li->children(1)->plaintext;
                                    $course[$type . 'Link'] = $li->children(1)->href;
                                }
                                unset($type);
                            }elseif($li->children(0)->tag == 'a'){
                                $course['Unit'] = $li->children(0)->plaintext;
                                $course['UnitLink'] = 'http://www.hse.ru' . $li->children(0)->href;
                            }
                        }
                        $ul->clear();
                    }
                    unset($ul);
                    $modules->clear();
                    $title->clear();
                    unset($modules);
                    unset($title);
                    $courses[] = $course;
                    unset($course);
                }
            }
            $content->clear();
            $html->clear();
            unset($content);
            unset($html);
        }
        
        unset($urls);
        return $courses;
    }
    
    public function CoursesParseRu()
    {
        //флаг нужен для учёта пагинации
        $flag = true;
        $pageNumber = 1;
        $courses = [];
        while ($flag) {
            $html = file_get_html('https://www.hse.ru/edu/courses/page' . $pageNumber . '.html');
            if($html){
                $content = $html->find('div#maincontent',0);
                //в первых 4 потомках есть лишние div. Чтобы не прогонять цикл по лишним
                //нужно удалить первые 4 элемента. и остануться нужные div
                array_splice($content->children, 0, 4);
                $course = [];
                foreach ($content->children() as $row) {
                    $type = $row->children(0);
                    if($type){
                        if($type->tag == 'h2' && $type->class == 'doc'){
                            $course['Название'] = $type->children(0)->plaintext;
                            $course['Ссылка'] = $type->children(0)->href;
                        }elseif($type->tag == 'span' && $type->class == 'field'){
                            $course[$type->plaintext] = $row->nodes[1]->plaintext;
                        }elseif($row->class == 'data'){
                            $elements = $row->children();
                            foreach ($elements as $element) {
                                $elementType = trim($element->children(0)->plaintext,' :');
                                if(count($element->children()) == 1){
                                    if(isset($element->nodes[1])){
                                        $course[$elementType] = trim($element->nodes[1]->plaintext,' ');
                                    }
                                }elseif(count($element->children()) == 2){
                                    $course[$elementType] = $element->children(1)->plaintext;
                                    $course[$elementType . ' Cсылка'] = $element->children(1)->href;
                                }elseif(count($element->children()) == 3 && $element->children(1)->tag == 'a' && $element->children(1)->tag == 'a' ){
                                    //тут наиболее вероятно прогр.уч.дисцп
                                    $course[$elementType] = 'http://www.hse.ru' .$element->children(2)->href;
                                }else{
                                    //сюда попадёт только: Преподаватели.
                                    $instructors = $element->find('a');
                                    foreach ($instructors as $instructor) {
                                        $course[$elementType][] =[
                                            'Имя' => $instructor->plaintext,
                                            'Ссылка' => 'http://www.hse.ru' . $instructor->href,
                                        ] ;
                                    }
                                    $instructors->clear();
                                    unset($instructors);
                                }
                                unset($elementType);
                            }
                            $courses[] = $course;
                            $elements->clear();
                            unset($elements);
                            unset($course);
                        }
                        $type->clear();
                    }
                    unset($type);
                }
                
                $flag = false;
                foreach($content->find('div.letterlist',0)->find('a') as $link) {
                    if((int)$link->plaintext == $pageNumber+1){
                        $flag = true;
                        $pageNumber++;
                    }
                }
                $content->clear();
                $html->clear();
                unset($content);
            }
            unset($html);
        }
        return $courses;
    }
    
    private function coursesExportRu()
    {
        $courses = $this->institution['Courses'];
        $dom = new domDocument('1.0', 'utf-8');
        $dom->formatOutput = true;
        $xmlCourses = $dom->createElement('Courses');
        
        foreach($courses as $id=>$course){
            $xmlCourse = $dom->createElement('Course');
            $xmlCourse->setAttribute('Id', $id);
            
            foreach ($course as $attribute => $value) {
                $tag = $this->stringToTag($attribute);
                $courseChildren = $dom->createElement($tag);
                
                if(!is_array($value)){
                    //$courseChildren->setAttribute($tag, $value);
                    $courseChildren->nodeValue = (string) $value;
                }else{
                    foreach ($value as $element) {
                        if(is_array($element)){
                            $xmlChildren = $dom->createElement('Author');
                            foreach($element as $index=>$sign){
                                $xmlChildren->setAttribute($index, $sign);
                            }
                            $courseChildren->appendChild($xmlChildren);
                            unset($xmlChildren);
                        }
                    }
                }
                $xmlCourse->appendChild($courseChildren);
            }
            $xmlCourses->appendChild($xmlCourse);
        }
        $dom->appendChild($xmlCourses);
        $dom->save('xmlResult/CoursesRu.xml');
        unset($dom);
    }
    
    private function coursesExportEn()
    {
        $courses = $this->institution['Courses'];
        $dom = new domDocument('1.0', 'utf-8');
        $dom->formatOutput = true;
        $xmlCourses = $dom->createElement('Courses');
        
        foreach($courses as $id=>$course){
            $xmlCourse = $dom->createElement('Course');
            $xmlCourse->setAttribute('Id', $id);
            
            foreach ($course as $attribute => $value) {
                $tag = $this->stringToTag($attribute);
                $xmlCourse->setAttribute($tag, $value);
            }
            $xmlCourses->appendChild($xmlCourse);
        }
        $dom->appendChild($xmlCourses);
        $dom->save('xmlResult/CoursesEn.xml');
        unset($dom);
    }
    
    public function EmployeeParseEn()
    {
        $urls = false;
        $personUrlTemplate;
        if($this->language == 'en'){
            $urls = [
                'Moskow' => 'https://www.hse.ru/en/org/persons/?udept=22726',
                'Nizhny Novgorod' => 'https://www.hse.ru/en/org/persons/?udept=135288',
                'Saint Petersburg' => 'https://www.hse.ru/en/org/persons/?udept=135083',
                'Perm' => 'https://www.hse.ru/en/org/persons/?udept=135213',
            ];
            $personUrlTemplate = 'https://www.hse.ru/en/org/persons/';
        }else{
            $urls = [
                'Moskow' => 'https://www.hse.ru/org/persons/?udept=22726',
                'Nizhny Novgorod' => 'https://www.hse.ru/org/persons/?udept=135288',
                'Saint Petersburg' => 'https://www.hse.ru/org/persons/?udept=135083',
                'Perm' => 'https://www.hse.ru/org/persons/?udept=135213',
            ];
            $personUrlTemplate = 'https://www.hse.ru/org/persons/';
        }
        $found['All'] = 0;
        $added['All'] = 0;
        $crashedUrl = false;
        foreach ($urls as $city => $url) {
            $mainHtml = file_get_html($url);
            $urlTemplate = 'https://www.hse.ru';
            $mainContent = $mainHtml->find('div.abc-filter',0);
            $links = $mainContent->find('a');
            $found[$city] = 0;
            $added[$city] = 0;
            foreach ($links as $link) {
                
                if($html = file_get_html($personUrlTemplate . $link->href)){
                    $content = $html->find('div.persons__section',0);
                    foreach ($content->children() as $element) {
                        $personUrl = '';
                        if(count($element->children(0)->children())>1 ){
                            $personUrl = $urlTemplate . $element->children(0)->children(1)->children(0)->children(0)->href;
                        }else{
                            $personUrl = $urlTemplate . $element->children(0)->children(0)->children(0)->children(0)->href;
                        }
                        $found[$city]++;
                        $found['All']++;
                        if($employee = $this->personParse($personUrl)){
                            $employee['Campus'] = $city;
                            $this->institution['Employees'][] = $employee;
                            $added[$city]++;
                            $added['All']++;
                            unset($employee);
                        }else{
                            $crashedUrl[] = $personUrl;
                        }
                        unset($personUrl);
                        }
                    $content->clear();
                    $html->clear();
                    unset($content);
                    unset($html);
                    //echo '<br>' . '[foundlt]: ' . $found[$city] . '[addedlt]: ' . $added[$city] . PHP_EOL;
                    break;
                }
            }
            $mainContent->clear();
            $mainHtml->clear();
            unset($links);
            unset($mainContent);
            unset($mainHtml);
            echo '<br>' . '[found ' . $city . ']: ' . $found[$city] . '[added ' . $city . ']: ' . $added[$city] . PHP_EOL;
            //2я попытка добавить тех что не добавились
            //break if only moskow
            break;
        }
        echo '<br>' . '[foundAll]: ' . $found['All'] . '[addedAll]: ' . $added['All'] . PHP_EOL;
        if($crashedUrl){
            foreach ($crashedUrl as $url) {
                if($employee = $this->personParse($url)){
                    $employee['Campus'] = $city;
                    $this->institution['Employees'][] = $employee;
                    $added[$city]++;
                    $added['All']++;
                    unset($employee);
                }
            }
            echo '<br>' . '[found ' . $city . ']: ' . $found[$city] . '[added.N2' . $city . ']: ' . $added[$city] . PHP_EOL;
            unset($crashedUrl);
        }
        unset($urls);
    }
    
    private function personParse($url)
    {
        $urlTemplate = 'https://www.hse.ru';
        $person = false;
        if($html = file_get_html($url)){
            
            //MainContent
            $content = $html->find('div.main',0);
            
            //Person name
            $person['Name'] = (string) $content->children(0)->children(0)->plaintext;
            
            //Person link
            $person['Link'] = $url;
        
            //contacts column
            $contacts = $html->find('div.l-extra__inner',0);
        
            //Person Image
            $image = $contacts->find('div.g-pic',0);
            if(isset($image)){
                $person['ImageLink'] = $image->style;
                $image->clear();
            }
            unset($image);
        
            $list = $contacts->find('dl');
            if(isset($list)){
                foreach ($list as $dl) {
                   
                    //Language Proficiency
                    if($dl->class == 'main-list large main-list-language-knowledge-level'){
                        $languages = $dl->find('dd');
                        if(isset($languages)){
                            foreach($languages as $language){
                                $person['Language Proficiency'][] = $language->plaintext;
                                //$person[$type->plaintext][] = $language->plaintext;
                            }
                        }
                        unset($type);
                        unset($languages);
                    }
                
                    //Contacts
                    if($dl->class == 'main-list large'){
                        //$type = $dl->find('dt.b',0)->plaintext;
                        foreach ($dl->children() as $key=>$dd) {
                            if($key==0){continue;}
                            if(isset($dd->nodes)){
                                if($dd->nodes[0]->plaintext == 'E-mail:'|| $dd->nodes[0]->plaintext == 'Электронная почта:'){continue;}
                                foreach ($dd->nodes as $index=>$node) {
                                    if($index == 0 || $node->tag == 'br'){continue;}
                                    if($node->tag == 'span' && $node->class == 'grey'){
                                        if($adress = $this->adressStringToArray($dd->nodes[0]->plaintext)){
                                            $person['Contacts'][key($adress)] = current($adress);
                                            continue;
                                        }
                                    }
                                    $str = $node->plaintext;
                                    trim($str);
                                    $str = str_replace('&nbsp;', '', $str);
                                    //$person[$type][$dd->nodes[0]->plaintext][] = $str;
                                    $person['Contacts'][$dd->nodes[0]->plaintext][] = $str;
                                    unset($str);
                                }
                            }
                        }
                        unset($type);
                    }
                
                    //idList
                    if($dl->class == 'main-list person-extra-indent'){
                        foreach ($dl->children as $inf) {
                            if($inf->children(1)){
                                $person['idList'][$inf->children(0)->plaintext] = $inf->children(1)->plaintext;
                            }
                        }
                    }
                }
            }
            unset($list);
            unset($contacts);
            
            //Apointments
            //этот элемент в основном выглядит как
            //должность: факультет/кафедра
            //есть варианты когда кафедры нет. Ещё есть варианты когда нет ссылок на факультет/кафедру
            foreach ($content->children(0)->children(1)->children() as $element) {
                if(count($element->children())>1){
                    $name = $element->children(0);
                    $person['Appointments'][$name->plaintext]=[];
                    $faculty = $element->children(1);
                    $person['Appointments'][$name->plaintext]['Department']=$faculty->plaintext;
                    if($faculty->tag == 'a'){
                        $person['Appointments'][$name->plaintext]['DepartmentLink']=$faculty->href;
                    }
                    $unit = $element->children(2);
                    if(isset($unit)){
                        $person['Appointments'][$name->plaintext]['Unit']=$unit->plaintext;
                        if($unit->tag == 'a'){
                            $person['Appointments'][$name->plaintext]['UnitLink']=$unit->href;
                        }
                        $unit->clear();
                    }
                    unset($unit);
                    unset($faculty);
                }elseif(count($element->children())==1){
                    if(isset($element->nodes[1])){
                        $person['Appointments'][$element->children(0)->plaintext] = [
                            'Program'=>(string)$element->nodes[1]->href,
                        ];
                    }
                }
            }
            
            //Experience
            if($experience = $content->children(0)->children(2)){
                foreach ($experience->children() as $element) {
                    $person['Experience'] [] = $element->plaintext;
                }
            }
            
            if($personData = $content->find('div.main__inner',0)){
            
                //Home
            
                //Education and degrees
                if($edu = $personData->find('div[tab-node=sci-degrees1]',0)){
                    if($edu->children(1)){
                        if($edu->children(1)->tag =='ul'){
                            foreach ($edu->children(1)->children() as $li) {
                                $nodes = $li->find('p.first_child',0);
                                $year = $li->find('div.person-list-hangover',0);
                                if(isset($nodes) && isset($year)){
                                    $name = '';
                                    foreach($nodes->nodes as $node){
                                        if($node->tag== 'br' || $node->plaintext == '*'){continue;}
                                        $name .= $node->plaintext;
                                    }
                                    $person['Education and Degrees'][$year->plaintext] = $name;
                                }
                            }
                        }elseif($edu->children(1)->tag =='p'){
                            $year = $li->find('div.person-list-hangover',0);
                            $name = '';
                            foreach($edu->children(1)->nodes as $node){
                                if($node->tag== 'br'){continue;}
                                $name .= $node->plaintext;
                            }
                            $person['Education and Degrees'][$year->plaintext] = $name;
                        }
                    }
                    unset($edu);
                }
        
                //ProfInterests
                if($intrests = $personData->find('div[tab-node=sci-intrests]',0)){
                    foreach ($intrests->children(1)->children() as $value) {
                        $person['Professional Interests'] [] = $value->plaintext;
                    }
                    unset($intrests);
                }
            
                //Awards
                if($awardsList = $personData->find('div[tab-node=awards]',0)){
                    if(count($awardsList->children())>1){
                        $awards = $awardsList->children(1)->children();
                        if(isset($awards)){
                            foreach ($awards as $value) {
                                $person['Awards'] [] = [
                                    'Name' => $value->plaintext,
                                    'Link' => $urlTemplate . $value->href,
                                ];
                            }
                        }
                        unset($awards);
                    }
                    $awardsList->clear();
                    unset($awardsList);
                }
            
                //Teaching
        
                //Courses
                if($courses = $personData->find('div[tab-node=edu-courses]',0)){
                    //курсы на страницах в разных ваиантах представлены...
                    //нужно учесть оба варианта:
                    //схема: название и квалификация в nodes[0]
                    //<a> - юнит. это nodes[1] или children[0]
                    //nodes[2] - программа
                    //спец. м.б ссылкой или текстом... или вовсе их может и не быть.
                    //подход  с разделением не очень. Лучше разбить на нужную инфу регулярками.
                    //echo PHP_EOL . $person['name'];
                    //$person['CoursesFound'] = 0;
                    if($courses->children(1)){
                        if ($courses->children(1)->tag == 'ul') {
                            foreach ($courses->children(1)->children() as $value) {
                                //$person['CoursesFound']++;
                                if($course = $this->courseInformation($value->plaintext)){
                                    $person['Courses'][]= $course;
                                    unset($course);
                                }
                            }
                        }elseif($courses->children(1)->tag == 'div'){
                            //$person['CoursesFound']++;
                            if($course = $this->courseInformation($courses->children(1)->plaintext)){
                                $person['Courses'][]= $course;
                                unset($course);
                            }
                        }
                    }
                    $courses->clear();
                    unset($courses);
                }
            
                //StudentTerm|thesisPaper
                if($term = $personData->find('div[tab-node=vrs]',0)){
                    $level = '';
                    foreach ($term->children(1)->children() as $li) {
                        if($li->class == 'i' || $li->class == 'i not_display'){
                            $level = $li->plaintext;
                        }else{
                            $link = $li->find('a.link',0);
                            $paper['Name'] = $li->plaintext;
                            if(isset($level)){
                                $paper['Level'] = $level;
                            }
                            if(isset($link)){
                                $paper['Link'] = $link->href;
                            }
                            $person['Student Term'] [] = $paper;
                            unset($link);
                            unset($paper);
                        }
                    }
                    $term->clear();
                    unset($level);
                    unset($term);
                }
        
                //Editorial Board Membership
                if($editorialStaff = $personData->find('div[tab-node=editorial-staff]',0)){
                    if($editorialStaff->children(1)){
                        foreach ($editorialStaff->children(1)->children() as $value) {
                            $person['Editorial Board Membership'] [] = $value->plaintext;
                        }
                    }
                    $editorialStaff->clear();
                    unset($editorialStaff);
                }
                
                //Research and publications
                
                //Conferences
                if($conferences = $personData->find('div[tab-node=conferences]',0)){
                    
                    if($conferences->children(1)->tag == 'div'){
                        $conference = false;
                        if($conferences->children(1)->children(1)){
                            $conference['Name'] = $conferences->children(1)->children(1)->plaintext;
                        }else{
                            $conference['Name'] = $conferences->children(1)->nodes[0]->plaintext;
                        }
                        $conference['Year'] = $conferences->children(1)->children(0)->plaintext;
                        $person['Conferences'][] = $conference;
                    }elseif($conferences->children(1)->tag == 'ul'){
                        $year = false;
                        foreach ($conferences->children(1)->children() as $li) {
                            $conference = [];
                            if($li->children(0) && $li->children(0)->tag == 'div'){
                                $conference['Name'] = $li->nodes[0]->plaintext;
                                $year = $li->children(0)->plaintext;
                                if(!$li->find('span.file-small',0)){
                                    $conference['Name'] = $li->nodes[0]->plaintext;
                                }else{
                                    $conference['Name'] = $li->children(1)->nodes[0]->plaintext;
                                }
                            }elseif($li->children(0) && $li->children(0)->tag == 'p'){
                                $conference['Name'] = $li->children(0)->plaintext;
                            }else{
                                $conference['Name'] = $li->nodes[0]->plaintext;
                            }
                            if($year){
                                $conference['Year'] = $year;
                            }
                            $person['Conferences'][] = $conference;
                        }
                    }
                    unset($conferences);
                }
                
                //Publications
                if($publications = $personData->find('div[tab-node=pubs]')){
                    //Публикации на страницах в разных ваиантах представлены...
                    //нужно учесть оба варианта:
                    foreach($publications as $pubsList){
                        $year = null;
                        foreach($pubsList->children() as $children){
                            if($children->tag == 'h3'){
                                if(isset($children->nodes[0])){
                                    if(ctype_digit($children->nodes[0]->plaintext)){
                                        $year = $children->nodes[0]->plaintext;
                                    }
                                }
                            }elseif($children->tag == 'div'){
                                if($publication = $this->publication($children->children(0), $year)){
                                    $person['Publications'][] = $publication;
                                    unset($publication);
                                }
                            }elseif($children->tag = 'ul'){
                                foreach ($children->children() as $li) {
                                    if($publication = $this->publication($li->children(0), $year)){
                                        $person['Publications'][] = $publication;
                                        unset($publication);
                                    }
                                }
                            }
                        }
                    }
                    unset($publications);
                }
                $personData->clear();
            }
            unset($personData);
            //$this->institution['staff'][] = $person;
            //unset($person);
            unset($html);
        }
        return $person;
    }
    
    private function publication($element, $year)
    {
        $publication = null;
        if(isset($element) && $element->children()){
            foreach($element->children() as $children){
                if($children->class == 'nowrap'){
                    $author['Name'] = $children->plaintext;
                    if($authorLink = $children->find('a.link',0)){
                        $author['Name'] = $authorLink->plaintext;
                        $author['Link'] = $authorLink->href;
                    }
                    $publication['Authors'][] = $author;
                }elseif($children->class == 'i'){
                    $publication['Type'] = $children->plaintext;
                }if($children->tag == 'a' && $children->class == 'link'){
                    $publication['Name'] = $children->plaintext;
                    $publication['Link'] = $children->href;
                }
            }
            if($year){
                $publication['Year'] = $year;
            }
        }
        return $publication;
    }
    
    private function employeeExport()
    {
        $employees = $this->institution['Employees'];
        $dom = new domDocument('1.0', 'utf-8');
        $dom->formatOutput = true;
        $xmlEmployees = $dom->createElement('Employees');
        
        foreach($employees as $id=>$employee){
            $xmlEmployee = $dom->createElement('Employee');
            $xmlEmployee->setAttribute('Id', $id);
            
            if(isset($employee['Name'])){
                $xmlEmployee->setAttribute('Name', $employee['Name']);
            }
            if(isset($employee['Campus'])){
                $xmlEmployee->setAttribute('Campus', $employee['Campus']);
            }
            if(isset($employee['Link'])){
                $xmlEmployee->setAttribute('Link', $employee['Link']);
            }
            if(isset($employee['ImageLink'])){
                $xmlEmployee->setAttribute('ImageLink', $employee['ImageLink']);
            }
            if(isset($employee['Language Proficiency'])){
                $languages = $employee['Language Proficiency'];
                $xmlElement = $dom->createElement('LanguageProficiency');
                foreach ($languages as $language) {
                    $xmlLanguage = $dom->createElement('Language');
                    $xmlLanguage->setAttribute('Name', $language);
                    $xmlElement->appendChild($xmlLanguage);
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['Contacts'])){
                $contacts = $employee['Contacts'];
                $xmlElement = $dom->createElement('Contacts');
                foreach ($contacts as $name => $value) {
                    if (is_array($value)) {
                        foreach ($value as $element) {
                            if(trim($element) == 'нет' || trim($element) == '* нет'){continue;}
                            $xmlContact = $dom->createElement('Contact');
                            $xmlContact->setAttribute('type', $name);
                            $xmlContact->nodeValue = $element;
                            $xmlElement->appendChild($xmlContact);
                        }
                    }
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['idList'])){
                $idList = $employee['idList'];
                $xmlElement = $dom->createElement('IdList');
                foreach ($idList as $name => $value) {
                    $xmlId = $dom->createElement($this->stringToTag($name), $value);
                    $xmlElement->appendChild($xmlId);
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['Apointments'])){
                $apointments = $employee['Apointments'];
                $xmlElement = $dom->createElement('Apointments');
                foreach ($apointments as $name => $value) {
                    $xmlApointment = $dom->createElement('Apointment');
                    $xmlApointment->setAttribute('Name', trim($name, ' :'));
                    if (is_array($value)) {
                        foreach ($value as $type => $element) {
                            $xmlApointment->setAttribute($this->stringToTag($type), $element);
                        }
                    }
                    $xmlElement->appendChild($xmlApointment);
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['Experience'])){
                $experience = $employee['Experience'];
                $xmlElement = $dom->createElement('Experience');
                foreach ($experience as $value) {
                    $xmlChildren = $dom->createElement('ExperienceChildren', $value);
                    $xmlElement->appendChild($xmlChildren);
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['Education and Degrees'])){
                $edu = $employee['Education and Degrees'];
                $xmlElement = $dom->createElement('EducationAndDegrees');
                foreach ($edu as $year => $degree) {
                    $xmlChildren = $dom->createElement('Degree');
                    $xmlChildren->setAttribute('year', $year);
                    $xmlChildren->setAttribute('name', $degree);
                }
                $xmlElement->appendChild($xmlChildren);
            }
            if(isset($employee['Professional Interests'])){
                $interests = $employee['Professional Interests'];
                $xmlElement = $dom->createElement('ProfessionalInterests');
                foreach ($interests as $value) {
                    $xmlChildren = $dom->createElement('ProfessionalInterest');
                    $xmlChildren->setAttribute('name', $value);
                    $xmlElement->appendChild($xmlChildren);
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['Courses'])){
                $courses = $employee['Courses'];
                $xmlCourses = $dom->createElement('Courses');
                foreach ($courses as $course) {
                    $xmlCourse = $dom->createElement('Course');
                    foreach ($course as $name => $value) {
                        $xmlCourse->setAttribute($name, $value);
                    }
                    $xmlCourses->appendChild($xmlCourse);
                }
                $xmlEmployee->appendChild($xmlCourses);
            }
            if(isset($employee['Conferences'])){
                $conferences = $employee['Conferences'];
                $xmlElement = $dom->createElement('Conferences');
                foreach ($conferences as $conference) {
                    $xmlChildren = $dom->createElement('Conferences');
                    if(isset($conference['Name'])){
                        $xmlChildren->setAttribute('Name', $conference['Name']);
                    }
                    if(isset($conference['Year'])){
                        $xmlChildren->setAttribute('Year', $conference['Year']);
                    }
                    $xmlElement->appendChild($xmlChildren);
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['Awards and Accomplishments'])){
                $awards = $employee['Awards and Accomplishments'];
                $xmlElement = $dom->createElement('Awards');
                foreach ($awards as $value) {
                    $xmlChildren = $dom->createElement('Award');
                    if(isset($value['Name'])){
                        $xmlChildren->setAttribute('Name', $value['Name']);
                    }
                    if(isset($value['Link'])){
                        $xmlChildren->setAttribute('Link', $value['Link']);
                    }
                    $xmlElement->appendChild($xmlChildren);
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['Student Term'])){
                $terms = $employee['Student Term'];
                $xmlElement = $dom->createElement('StudentTerms');
                foreach ($terms as $term) {
                    $xmlChildren = $dom->createElement('StudentTerm');
                    if(isset($term['Name'])){
                        $xmlChildren->setAttribute('Name', $term['Name']);
                    }
                    if(isset($term['Level'])){
                        $xmlChildren->setAttribute('Link', $term['Level']);
                    }
                    if(isset($term['Link'])){
                        $xmlChildren->setAttribute('Link', $term['Link']);
                    }
                    $xmlElement->appendChild($xmlChildren);
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['Editorial Board Membership'])){
                $membership = $employee['Editorial Board Membership'];
                $xmlElement = $dom->createElement('EditorialBoardMembership');
                foreach ($membership as $value) {
                    $xmlChildren = $dom->createElement('BoardMember');
                    $xmlChildren->setAttribute('name', $value);
                    $xmlElement->appendChild($xmlChildren);
                }
                $xmlEmployee->appendChild($xmlElement);
            }
            if(isset($employee['Publications'])){
                $publications = $employee['Publications'];
                $xmlPublications = $dom->createElement('Publications');
                foreach ($publications as $publication) {
                    $xmlPublication = $dom->createElement('Publication');
                    foreach ($publication as $attr=>$value) {
                        if(!is_array($value)){
                            $xmlPublication->setAttribute($attr, $value);
                        }else{
                            $xmlAuthors = $dom->createElement('Authors');
                            foreach ($value as $element) {
                                $xmlAuthor = $dom->createElement('Author');
                                if(isset($element['Name'])){
                                    $xmlAuthor->setAttribute('Name', $element['Name']);
                                }
                                if(isset($element['Link'])){
                                    $xmlAuthor->setAttribute('Link', $element['Link']);
                                }
                                $xmlAuthors->appendChild($xmlAuthor);
                            }
                            $xmlPublication->appendChild($xmlAuthors);
                        }
                    }
                    $xmlPublications->appendChild($xmlPublication);
                }
                $xmlEmployee->appendChild($xmlPublications);
            }
            
            $xmlEmployees->appendChild($xmlEmployee);
        }
        $dom->appendChild($xmlEmployees);
        if($this->language == 'en'){
            $dom->save('xmlResult/Employee.xml');
        }else{
            $dom->save('xmlResult/EmployeeRu.xml');
        }
        unset($dom);
    }
    
    private function courseInformation($str)
    {
        $result = false;
        if(preg_match('/^([A-Za-zА-Яа-я" ]+)[(]/u', $str, $matches)){
            $result['Name'] = $matches[1];
        }
        if(preg_match('/[(]{1}([ A-Za-z]+)[’sprograme ]+/u', $str, $matches) || preg_match('/[(]{1}(["\- А-Яа-яA-Za-z]+);/u', $str, $matches)){
            $result['Level'] = $matches[1];
        }
        if(preg_match('/;[programe ]+"([\w ]+)"/u', $str, $matches) || preg_match('/;[программа ]+"([\w ]+)"/u', $str, $matches)){
            $result['ProgramName'] = $matches[1];
        }
        if(preg_match('/;([\-0-9\, ]+)[year]{4}/u', $str, $matches) || preg_match('/;([\-0-9 ]+)[ \-йкурс]{7}/u', $str, $matches)){
            $result['Year'] = $matches[1];
        }
        if(preg_match('/([\-0-9\, ]+)[module]{6}/u', $str, $matches) || preg_match('/([\-0-9\, ]+)[модуль]{6}/u', $str, $matches)){
            $str = trim($matches[1],' :,-');
            $result['Modules'] = $str;
        }
        if(preg_match('/([\-0-9\, ]+)[semester]{8}/u', $str, $matches) || preg_match('/([\-0-9\, ]+)[семестр]{7}/u', $str, $matches)){
            $result['Semester'] = $matches[1];
        }
        if(preg_match('/;[\:гдечитается ]{14}(["\-А-Яа-яA-Za-z ]+);/u', $str, $matches)){
            $result['Unit'] = $matches[1];
        }
        return $result;
    }
    
    private function programsExport()
    {
        $programs = $this->getPrograms();
        foreach($programs as $id=>$program){
            $dom = new domDocument('1.0', 'utf-8');
            $dom->formatOutput = true;
            $xmlProgram = $dom->createElement('Program');
            $xmlProgram->setAttribute('Id', $id);
            $xmlProgram->setAttribute('Name', $program['Name']);
            $xmlProgram->setAttribute('Specialization', $program['Specialization']);
            $xmlProgram->setAttribute('Unit', $program['Unit']);
            $xmlProgram->setAttribute('Duration', $program['Duration']);
            $xmlProgram->setAttribute('Campus', $program['Campus']);
            $xmlProgram->setAttribute('Link', $program['Link']);
            if(isset($program['Courses'])){
                $xmlCourses = $dom->createElement('courses');
                foreach ($program['Courses'] as $course) {
                    $xmlCourse = $dom->createElement('Course');
                    $xmlCourse->setAttribute('Name', $course['Name']);
                    $xmlCourse->setAttribute('Status', $course['Status']);
                    if(isset($course['Year'])){
                        $xmlCourse->setAttribute('Year', $course['Year']);
                    }
                    if(isset($course['Module'])){
                        $xmlCourse->setAttribute('Modules', $course['Module']);
                    }
                    if(isset($course['Semester'])){
                        $xmlCourse->setAttribute('Semesters', $course['Semester']);
                    }
                    if(isset($course['Link'])){
                        $xmlCourse->setAttribute('Url', $course['Link']);
                    }
                    $xmlCourses->appendChild($xmlCourse);
                }
                $xmlProgram->appendChild($xmlCourses);
            }
            $dom->appendChild($xmlProgram);
            if($this->language == 'en'){
                $dom->save('xmlResult/programs/' . $this->stringToTag($program['Name']) . '.xml');
            }else{
                //вместо имени id т.к кирилица не отображается.
                $dom->save('xmlResult/programsRu/' . $this->stringToTag($id) . '.xml');
            }
            unset($dom);
        }
    }
    
    private function CourseElement($index,$element)
    {
        $type = trim((string)$element->nodes[0]->plaintext);
        switch ($type) {
            case 'Статус:':
                //echo 'status' . $value->nodes[1] . '<br>';
                //$element['status'] = $value->nodes[1];
                $this->courses[$index]['status'] = $element->nodes[1]->plaintext;
            break;
            case 'Кто читает:':
                //привязывать департаменты сразу по имени и/или ссылке?
                //чуть позднее реальзую
                echo $element->nodes[1]->href;
                $this->courses[$index]['department'] = $element->nodes[1]->plaintext;
                foreach ($this->institution['units'] as $key=>$unit) {
                    if($this->linksCompare($unit['link'],$element->nodes[1]->href)){
                        $this->courses[$index]['departmentInArray'] = $key; 
                    }
                }
            break;
            case 'Где читается:':
                //привязывать факультет как и департамент
                //чуть позднее реальзую
                //echo 'faq' . $value->nodes[2]->plaintext . '<br>';
                $this->courses[$index]['faculty'] = $element->nodes[2]->plaintext;
            break;
            case 'Язык:':
                //echo 'lang' . $value->nodes[1] . '<br>';
                $this->courses[$index]['language'] = $element->nodes[1]->plaintext;
            break;
            case 'Уровень:':
                //echo 'level' . $value->nodes[1] . '<br>';
                $this->courses[$index]['level'] = $element->nodes[1]->plaintext;
            break;
            case 'Направление:':
                //echo 'qual' . $value->nodes[1] . '<br>';
                $this->courses[$index]['qualification'] = $element->nodes[1]->plaintext;
            break;
            case 'Специализация:':
                //echo 'spec' . $value->nodes[1] . '<br>';
                $this->courses[$index]['specialisation'] = $element->nodes[1]->plaintext;
            break;
            case 'Когда читается:':
                //echo 'time' . $value->nodes[1] . '<br>';
                $this->courses[$index]['time'] = $element->nodes[1]->plaintext;
            break;
            case 'Кредитов:':
                //echo 'credits' . $value->nodes[1] . '<br>';
                $this->courses[$index]['credits'] = $element->nodes[1]->plaintext;
            break;
            case 'Образовательная программа:':
                //echo 'progr' . $value->nodes[1] . '<br>';
                $this->courses[$index]['programName'] = $element->nodes[2]->plaintext;

            break;
        }
    }
    
    private function linksCompare($link1,$link2)
    {
        preg_match('/[\/]{1,2}([\w.\/]+)/u', $link1,$matches);
        $link1 = $matches[1];
        $link2 = preg_match('/[\/]{1,2}([\w.\/]+)/u', $link2, $matches);
        $link2 = $matches[1];
        //echo '<br>' . $link2;
        unset($matches);
        $result = false;
        if($link1 == $link2){
            $result = true;
        }
        return $result;
    }
    
    private function unitType($str)
    {
        //faculty-1
        //department-2
        //institute-3
        //school-4
        //centre-5
        $type = 0;
        $elements = explode(' ', mb_strtolower(trim($str)));
        foreach($elements as $word){
            if($word == 'faculty'|| $word == 'факультет'){
                $type = 'faculty';
                break;
            }else if($word == 'department'|| $word == 'кафедра'|| $word == 'департамент'){
                $type = 'department';
                break;
            }else if($word == 'institute' || $word == 'институт' || $word == 'Institute'){
                $type = 'institute';
                break;
            }else if($word == 'school'|| $word == 'школа' || $word == 'лицей'){
                $type = 'school';
                break;
            }else if($word == 'centre'|| $word == 'центр'){
                $type = 'cente';
                break;
            }else if($word == 'office'|| $word == 'офис'){
                $type = 'office';
                break;
            }
        }
        return $type;
    }
    
    public function institutionInformation()
    {
        echo '<pre>';
        print_r($this->institution);
    }
    
    private function stringToTag($str)
    {
        $str = str_replace('&nbsp;', '', $str);
        $str = trim($str,' :');
        $str = mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
        $str = str_replace(' ', '', $str);
        return $str;
    }
    
    private function adressStringToArray($str)
    {
        $adress = explode(':', $str);
        $result[$adress[0]][] = $adress[1];
        return $result;
    }
    
    private function dlPage($url) 
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4");
        $str = curl_exec($curl);
        curl_close($curl);
        
        $dom = new simple_html_dom();
        $dom->load($str);
        return $dom;
    }
    
    public function programJson()
    {
        $jsonPrograms = json_encode($this->institution['Programs']);
        $file = 'programs.txt';
        file_put_contents($file, $jsonPrograms);
    }
}